<?php
/**
 * Created by PhpStorm.
 * User: werewolf
 * Date: 02.08.2018
 * Time: 16:15
 */

namespace werewolf8904\composite;


use werewolf8904\composite\service\BaseService;

interface IServiceFactory
{
    public function getService($name): BaseService;
}