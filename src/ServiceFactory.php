<?php
/**
 * Created by PhpStorm.
 * User: werewolf
 * Date: 02.08.2018
 * Time: 16:14
 */

namespace werewolf8904\composite;


use werewolf8904\composite\service\BaseService;
use Yii;
use yii\base\InvalidConfigException;

class ServiceFactory implements IServiceFactory
{

    protected $_map;

    /**
     * ServiceFactory constructor.
     *
     * ```php
     *           [
     *             key =>[
     *              'class' => BaseService::class,
     *              'model_class' => CompositeForm::class,
     *              'main_model' => 'modelProduct',
     *              'main_model_class' => CatalogProduct::class,
     *              'relations' =>
     *                       [
     *                         'modelsCategory' => 'catalogProductToCategories',
     *                         'modelsRelated' => 'catalogProductsRelateds',
     *                         'modelsCharacteristic' => 'catalogProductCharacteristics',
     *                         'modelsImage' => 'images',
     *                         'modelsDiscount' => 'catalogProductDiscounts'
     *                       ]
     *                   ]
     *            ]
     * ```
     *
     * @param $map
     */
    public function __construct($map)
    {
        $this->_map = $map;
    }

    /**
     * Get Service instance by key from constructor map array
     * If not mapping found return Yii::createObject($name)
     *
     * @param $name string
     *
     * @return \werewolf8904\composite\service\BaseService
     * @throws \yii\base\InvalidConfigException
     */
    public function getService($name): BaseService
    {

        $data = $name;
        if (array_key_exists($name, $this->_map)) {
            $data = $this->_map[$name];
        }
        $service = Yii::createObject($data);
        if ($service instanceof BaseService) {
            return $service;
        }
        throw new InvalidConfigException('Must be instanceof BaseService');

    }
}