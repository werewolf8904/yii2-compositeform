<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 03.12.2017
 * Time: 20:00
 */

namespace werewolf8904\composite;


/**
 * Interface LoadSortable
 *
 * @package common\models\interfaces
 */
interface ILoadSortable
{

    /**
     * @return mixed
     */
    public function getSortField();

    /**
     * @param $val
     *
     * @return mixed
     */
    public function setSortField($val);
}