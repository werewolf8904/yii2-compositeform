<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 05.12.2017
 * Time: 14:20
 */

namespace werewolf8904\composite\service;


use werewolf8904\composite\base\CompositeForm;
use yii\base\Component;
use yii\base\Exception;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * Class BaseService
 *
 * @package backend\compositeform\service
 */
class BaseService extends Component
{
    public const EVENT_AFTER_SAVE = 'afterSave';
    public const EVENT_BEFORE_SAVE = 'beforeSave';
    public const EVENT_BEFORE_VALIDATE = 'beforeValidate';

    /**
     * ```php
     * function($query)
     * {
     * return $query
     * }
     *```
     *
     * @var \Closure
     */
    public $main_query_select_filter;

    /**
     * ```php
     * function($query,$relation)
     * {
     * return $query
     * }
     *```
     *
     * @var \Closure
     */
    public $relation_query_select_filter;

    /**
     * Composite form class
     *
     * ```php
     * $model_class=CompositeForm::class
     *```
     *
     * @var string
     */
    public $model_class;

    /**
     * Main model key for CompositeForm models property
     *
     * ```php
     * $main_model='modelProduct'
     *```
     *
     * @var string
     */
    public $main_model = '';

    /**
     * Main model class
     *
     * For example,
     *
     * ```php
     * 'main_model_class' = CatalogProduct::class
     * ```
     *
     * @var string
     */
    public $main_model_class = '';

    /**
     * Main model relations
     * key-key from CompositeForm models
     * value-name of main model relation
     *
     *```php
     *                      [
     *                         'modelsCategory' => 'catalogProductToCategories',
     *                         'modelsRelated' => 'catalogProductsRelateds',
     *                         'modelsCharacteristic' => 'catalogProductCharacteristics',
     *                         'modelsImage' => 'images',
     *                         'modelsDiscount' => 'catalogProductDiscounts'
     *                       ]
     *```
     *
     * @var array
     */
    public $relations = [];

    /**
     * Config for composite form
     * This array passed to composite constructor
     * ```php
     * [
     * 'not_copy_parameters' => ['key']
     * ]
     * ```
     *
     * @var array
     */
    public $composite_init_config = [];

    /**
     * Create copy of composite and saves
     *
     * @param $id
     *
     * @return bool whether is copied successfully
     * @throws \yii\db\Exception
     */
    public function copy($id)
    {
        $model = $this->findToCopy($id);
        if ($model) {
            return $this->save($model);
        }
        return false;
    }

    /**
     * Create composite copy
     *
     * @param $id
     *
     * @return CompositeForm|null copy of composite or null if source composite not exist
     */
    public function findToCopy($id)
    {
        $model = $this->find($id);
        /**
         * @var $model CompositeForm
         */
        if ($model) {
            $model->prepareToCopy();
        }

        return $model;
    }

    /**
     * Find composite by PK or conditions
     *
     * @param      $id array|string
     *
     * @return CompositeForm|bool composite or false if not find
     */
    public function find($id)
    {
        /**
         * @var $class ActiveRecord
         */
        $class = $this->main_model_class;
        $conditions = \is_array($id) ? $id : ['id' => $id];
        $query = $class::find()->andWhere($conditions);

        $query = ($this->main_query_select_filter instanceof \Closure) ? \call_user_func($this->main_query_select_filter, $query) : $query;
        foreach ($this->relations as $re) {
            $query->with([$re => function ($query) use ($re) {
                return ($this->relation_query_select_filter instanceof \Closure) ? \call_user_func($this->relation_query_select_filter, $query, $re) : $query;
            }]);
        }
        if (($model = $query->one()) !== null) {
            $form_model = $this->initComposite([$this->main_model => $model]);
            foreach ($this->relations as $name => $relation) {
                $form_model->$name = $model->$relation;
            }
            return $form_model;

        }

        return false;
    }

    protected function initComposite($data)
    {
        /**
         * @var $composite CompositeForm
         */
        $composite = new $this->model_class($this->composite_init_config);
        if ($composite->needInitialize) {
            /**
             * @var $main_model ActiveRecord
             */
            $main_model = new $this->main_model_class;
            $models = [$this->main_model => null];
            $modelsClass = [$this->main_model => $this->main_model_class];
            foreach ($this->relations as $key => $relation) {

                $relation = $main_model->getRelation($relation);
                $class = $relation->modelClass;
                $models[$key] = [];
                $modelsClass[$key] = $class;
            }
            $composite->modelsClass = $modelsClass;
            $composite->models = $models;
            $composite->mainModel = $this->main_model;
        }
        foreach ($data as $key => $value) {
            $composite->$key = $value;
        }
        return $composite;
    }

    /**
     * @param CompositeForm $model
     *
     * @return bool
     * @throws \yii\base\InvalidArgumentException
     * @throws \yii\base\InvalidCallException
     * @throws \yii\db\Exception
     */
    public function save($model)
    {
        $this->beforeValidate($model);
        /**
         * @var $main_model ActiveRecord
         */
        $main_model = $model->{$this->main_model};
        $valid = $main_model->validate();

        $is_new = $main_model->getIsNewRecord();
        //Run only main model validator and CompositeForm validators,
        if ($valid && $model->validate(null, true, true)) {
            $this->beforeSave($model);
            $transaction = \Yii::$app->db->beginTransaction();

            try {
                $flag = $main_model->save();
                if ($flag) {

                    foreach ($this->relations as $name => $relation) {
                        if ($model->$name !== false) {
                            $delIds = [];
                            $relation_id = $main_model->getRelation($relation)->link;
                            /**
                             * @var $class ActiveRecord
                             */
                            $class = $model->modelsClass[$name];
                            $keys = $class::primaryKey();
                            foreach ($keys as $key => $id) {
                                $oldIDs[$id] = ArrayHelper::map($main_model->$relation, $id, $id);
                                if (array_key_exists($id, $relation_id)) {
                                    $delIds[$id] = $oldIDs[$id];
                                } else {
                                    $delIds[$id] = array_diff($oldIDs[$id], array_filter(ArrayHelper::map($model->$name, $id, $id)));
                                }

                            }

                            if (!empty($delIds) && !$is_new) {
                                /**
                                 * @var $tmp ActiveRecord
                                 */
                                $tmp = $model->modelsClass[$name];
                                $tmp::deleteAll($delIds);
                            }
                            foreach ($model->$name as $mod) {
                                /**
                                 * @var $mod ActiveRecord
                                 */
                                foreach ($relation_id as $fk => $pk) {
                                    $mod->$fk = $main_model->$pk;
                                }
                                $valid = $mod->validate();
                                //Dont try linkink, if previous models already not linked, only validate all models for error reporting
                                if ($flag) {
                                    if ($valid) {
                                        $main_model->link($this->relations[$name], $mod);
                                    } else {
                                        $mod->isNewRecord = false;
                                        if ($mod->validate()) {
                                            $main_model->link($this->relations[$name], $mod);
                                        } else {
                                            $flag = false;
                                        }

                                    }
                                }

                            }
                        }
                    }
                }
                if ($flag) {
                    $this->afterSave($model);
                    $transaction->commit();
                    return true;

                }
            } catch (Exception $e) {
                $main_model->addErrors(['base' => $e->getMessage()]);
                $main_model->setIsNewRecord($is_new);
                $transaction->rollBack();
                return false;
            }
            $transaction->rollBack();
        } else {
            return false;
        }

        return false;

    }

    /**
     * @param CompositeForm $model
     */
    public function beforeValidate($model)
    {
        $event = new ServiceEvent(['model' => $model]);
        $this->trigger(self::EVENT_BEFORE_VALIDATE, $event);
    }

    /**
     * @param CompositeForm $model
     */
    public function beforeSave($model)
    {
        $event = new ServiceEvent(['model' => $model]);
        $this->trigger(self::EVENT_BEFORE_SAVE, $event);
    }

    /**
     * @param CompositeForm $model
     */
    public function afterSave($model)
    {
        $event = new ServiceEvent(['model' => $model]);
        $this->trigger(self::EVENT_AFTER_SAVE, $event);
    }

    /**
     * @param      $id
     * @param null $default_params
     *
     * @return mixed
     */
    public function findOrCreate($id, $default_params = null)
    {
        $model = $this->find($id);
        return $model ?: $this->create($default_params);
    }

    /**
     * @param null $default_params
     *
     * @return mixed
     */
    public function create($default_params = null)
    {
        $model = $default_params ? new $this->main_model_class($default_params) : new $this->main_model_class;
        return $this->initComposite([$this->main_model => $model]);
    }

    /**
     * @param $id
     *
     * @return bool
     * @throws \yii\db\Exception
     */
    public function delete($id)
    {
        $model = $this->find($id);
        if ($model) {
            $transaction = \Yii::$app->db->beginTransaction();
            try {
                foreach ($this->relations as $relation) {
                    $model->{$this->main_model}->unlinkAll($relation, true);
                }
                $model->{$this->main_model}->delete();
            } catch (\Exception $e) {
                $transaction->rollBack();
                return false;
            }
            $transaction->commit();
            return true;
        }
        return false;
    }

}