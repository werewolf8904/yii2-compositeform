<?php
/**
 * Created by PhpStorm.
 * User: werewolf
 * Date: 02.08.2018
 * Time: 16:41
 */

namespace werewolf8904\composite\service;


use yii\base\Event;

class ServiceEvent extends Event
{

    public $isValid = true;
    public $model;
}