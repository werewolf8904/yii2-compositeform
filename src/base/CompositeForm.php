<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 27.10.2017
 * Time: 9:17
 */

namespace werewolf8904\composite\base;


use werewolf8904\composite\ILoadSortable;
use yii\base\InvalidConfigException;
use yii\base\Model;
use yii\base\UnknownMethodException;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * Class Composite Form
 *
 * @author  Yurik <werewolf8904@gmail.com>
 * @package backend\modules\catalog\models
 */
class CompositeForm extends Model
{
    /**
     * Is models classes and storage must be initialized by Service
     *
     * @var bool
     */
    public $needInitialize = true;

    /**
     * Key of model from models with is main
     * This its fields can be direct accessed e.t CompositeForm->mainModelFieldName
     *
     * ```php
     * 'modelProduct'
     * ```
     *
     * @var string
     */
    public $mainModel;

    /**
     *
     * @var array
     */
    public $load_mapping = [];

    /**
     * All AR models stored her
     * key=>model|model[]
     *
     * ```php
     *  [
     *    'modelProduct' => null,
     *    'modelsCategory' => [],
     *    'modelsRelated'=>[],
     *    'modelsCharacteristic'=>[],
     *    'modelsImage'=>[],
     *    'modelsDiscount'=>[]
     *  ]
     *```
     *
     * @var array
     */
    public $models = [];

    /**
     * AR classes of stored models
     * Key is the same as in $models
     * key=>class name
     *
     *```php
     *  [
     *   'modelProduct' => Product::class,
     *   'modelsCategory' => CatalogProductToCategory::class,
     *   'modelsRelated' => CatalogProductsRelated::class,
     *   'modelsCharacteristic' => CatalogProductCharacteristic::class,
     *   'modelsImage' => CatalogProductImages::class,
     *   'modelsDiscount' => CatalogProductDiscount::class
     *  ]
     * ```
     *
     * @var array
     */
    public $modelsClass = [];

    /**
     * Parameters than will not be copied by copy method
     *```php
     *   [
     *     'sku',
     *     [
     *       'modelProduct'=>'id_1c'
     *     ]
     *   ]
     * ```
     *
     * @var array
     */
    public $not_copy_parameters = [];


    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        $labels = [];
        $keys = array_keys($this->models);
        foreach ($keys as $key) {
            $labels[$key] = $this->generateAttributeLabel($key);
        }
        return $labels;
    }


    /**
     * Prepare model to copy
     * Unset not copied parameters and set isNewRecord to true
     */
    public function prepareToCopy()
    {
        $all_models_params = [];
        foreach ($this->not_copy_parameters as $par) {
            if (\is_array($par)) {
                foreach ($par as $mod => $val) {
                    unset($this->models[$mod][$val]);
                }
            }
            if (\is_string($par)) {
                $all_models_params[] = $par;
            }
        }
        foreach ($this->models as $model) {
            if (\is_array($model)) {
                foreach ($model as $m) {
                    /**
                     * @var ActiveRecord $m
                     */
                    foreach ($all_models_params as $par) {
                        if ($m->hasAttribute($par)) {
                            unset($m->$par);
                        }
                    }
                    $m->setIsNewRecord(true);
                }
            } else {
                /**
                 * @var ActiveRecord $model
                 */
                foreach ($all_models_params as $par) {
                    if ($model->hasAttribute($par)) {
                        unset($model->$par);
                    }
                }
                $model->setIsNewRecord(true);
            }
        }
    }

    /**
     * Creates and populates a set of models.
     *
     * @param string $modelClass
     * @param array  $multipleModels
     * @param        $post
     *
     * @return array
     */
    public static function createMultiple($modelClass, $post, array $multipleModels = [])
    {
        /**
         * @var $model ActiveRecord
         */
        $model = new $modelClass;

        $models = [];

        $pk = $model::primaryKey();

        $keys_main = [];
        if (!empty($multipleModels)) {

            foreach ($pk as $k => $v) {
                $keys[$k] = ArrayHelper::getColumn($multipleModels, $v);

                $keys_main = array_map(function ($v, $k) {
                    return $v . '_' . $k;
                }, $keys_main, $keys[$k]);

            }

            $multipleModels = array_combine($keys_main, $multipleModels);
        }

        if ($post && \is_array($post)) {
            foreach ($post as $item) {
                $new = false;
                $key = '';
                foreach ($pk as $k => $v) {
                    if (ArrayHelper::getValue($item, $v) !== null) {
                        $key .= '_' . $item[$v];
                        $new = false;
                    } else {
                        $new = true;
                        break;
                    }

                }
                if (!ArrayHelper::getValue($multipleModels, $key)) {
                    $new = true;
                }
                if ($new) {
                    $models[] = new $modelClass;
                } else {
                    $models[] = $multipleModels[$key];
                }
            }
        }

        unset($model, $formName);

        return $models;
    }


    /**
     * @return string
     * @throws InvalidConfigException
     * @throws \ReflectionException
     */
    public function formName()
    {
        $first = reset($this->modelsClass);
        $reflector = new \ReflectionClass($first);
        if (PHP_VERSION_ID >= 70000 && $reflector->isAnonymous()) {
            throw new InvalidConfigException('The "formName()" method should be explicitly defined for anonymous models');
        }
        return $reflector->getShortName();
    }

    public function __call($name, $params)
    {
        try {
            return parent::__call($name, $params);
        } catch (UnknownMethodException $e) {
            return \call_user_func([$this->models[$this->mainModel], $name], $params);
        }


    }

    /**
     * @param string $name
     *
     * @return mixed
     * @throws \yii\base\InvalidCallException
     * @throws \yii\base\UnknownPropertyException
     */
    public function __get($name)
    {
        if (array_key_exists($name, $this->models)) {
            return $this->models[$name];
        }

        if ($this->mainModel && array_key_exists($this->mainModel, $this->models) && $this->models[$this->mainModel]->canGetProperty($name)) {
            return $this->models[$this->mainModel][$name];
        }
        return parent::__get($name);
    }

    /**
     * @param string $name
     * @param mixed  $value
     *
     * @throws \yii\base\InvalidCallException
     * @throws \yii\base\UnknownPropertyException
     */
    public function __set($name, $value)
    {

        if (array_key_exists($name, $this->models)) {
            $this->models[$name] = $value;
        } else {
            parent::__set($name, $value);
        }
    }

    /**
     * @param string $name
     *
     * @return bool
     */
    public function __isset($name)
    {
        return isset($this->models[$name]) || parent::__isset($name);
    }

    /**
     * @param null $attribute
     *
     * @return bool
     */
    public function hasErrors($attribute = null)
    {
        if ($attribute !== null && mb_strpos($attribute, '.') === false) {
            return parent::hasErrors($attribute);
        }
        if (parent::hasErrors($attribute)) {
            return true;
        }
        foreach ($this->models as $name => $form) {
            if ($this->$name !== false) {
                if (\is_array($form)) {
                    foreach ($form as $i => $item) {
                        if ($attribute === null) {
                            if ($item->hasErrors()) {
                                return true;
                            }
                        } elseif (mb_strpos($attribute, $name . '.' . $i . '.') === 0) {
                            if ($item->hasErrors(mb_substr($attribute, mb_strlen($name . '.' . $i . '.')))) {
                                return true;
                            }
                        }
                    }
                } else {
                    if ($attribute === null) {
                        if ($form->hasErrors()) {
                            return true;
                        }
                    } elseif (mb_strpos($attribute, $name . '.') === 0) {
                        if ($form->hasErrors(mb_substr($attribute, mb_strlen($name . '.')))) {
                            return true;
                        }
                    }
                }
            }
        }
        return false;
    }

    /**
     * @param null $attribute
     *
     * @return array
     */
    public function getErrors($attribute = null)
    {
        $result = parent::getErrors($attribute);
        foreach ($this->models as $name => $form) {
            if ($this->$name !== false) {
                if (\is_array($form)) {
                    /** @var array $form */
                    foreach ($form as $i => $item) {
                        foreach ($item->getErrors() as $attr => $errors) {
                            /** @var array $errors */
                            $errorAttr = $name . '.' . $i . '.' . $attr;
                            if ($attribute === null) {
                                foreach ($errors as $error) {
                                    $result[$errorAttr][] = $error;
                                }
                            } elseif (($errorAttr === $attribute) || $name === $attribute) {
                                foreach ($errors as $error) {
                                    $result[] = $error;
                                }
                            }
                        }
                    }
                } else {
                    foreach ($form->getErrors() as $attr => $errors) {
                        /** @var array $errors */
                        $errorAttr = $name . '.' . $attr;
                        if ($attribute === null) {
                            foreach ($errors as $error) {
                                $result[$errorAttr][] = $error;
                            }
                        } elseif ($errorAttr === $attribute) {
                            foreach ($errors as $error) {
                                $result[] = $error;
                            }
                        }
                    }
                }
            }
        }
        return array_unique($result, SORT_REGULAR);
    }

    /**
     * @param string $attribute
     *
     * @return mixed|string
     */
    public function getFirstError($attribute)
    {


        $err2 = str_replace([']', '['], ['', '.'], $attribute);
        $err = $this->getErrors($err2);
        return reset($err);

    }

    /**
     * @return array
     */
    public function getFirstErrors()
    {
        $result = parent::getFirstErrors();
        foreach ($this->models as $name => $form) {
            if ($this->$name !== false) {
                if (\is_array($form)) {
                    foreach ($form as $i => $item) {
                        foreach ($item->getFirstErrors() as $attr => $error) {
                            $result[$name . '.' . $i . '.' . $attr] = $error;
                        }
                    }
                } else {
                    foreach ($form->getFirstErrors() as $attr => $error) {
                        $result[$name . '.' . $attr] = $error;
                    }
                }
            }
        }
        return array_unique($result);
    }


    /**
     * @param array $data
     * @param null  $formName
     *
     * @return bool
     * @throws \yii\base\InvalidConfigException
     * @throws \ReflectionException
     */
    public function load($data, $formName = null)
    {
        $scope = $formName ?? $this->formName();
        $success = parent::attributes() ? parent::load($data, $scope) : !empty($data);

        if ($success) {
            foreach ($this->models as $name => $model) {
                $form = $this->$name;

                if (\is_array($form)) {
                    $originl_name = $name;
                    if (array_key_exists($name, $this->load_mapping)) {
                        $name = $this->load_mapping[$name];
                    }
                    if (array_key_exists($name, ($scope ? $data[$scope] : $data))) {
                        $input = $scope ? $data[$scope][$name] : $data[$name];
                        if ($input) {
                            $this->$originl_name = static::createMultiple($this->modelsClass[$originl_name], $input, $this->$originl_name);
                            if (\count($this->$originl_name) > 0) {

                                $form_data = array_values($input);
                                $success = static::loadMultiple($this->$originl_name, $form_data, '') && $success;
                                $tmp = $this->$originl_name;
                                if (reset($tmp) instanceof ILoadSortable) {
                                    $j = 0;
                                    /**
                                     * @var $tmp2 \werewolf8904\composite\ILoadSortable[]
                                     */
                                    foreach ($form_data as $i => $m) {
                                        $tmp2 = $this->$originl_name;
                                        $tmp2[$i]->setSortField(-$j);
                                        $j++;
                                    }
                                }
                            }

                        } else {
                            $this->$originl_name = [];
                        }
                    }
                } else {

                    $success = $form->load($data, $scope) && $success;
                }
            }
        }

        return $success;
    }

    /**
     * @param null $attributeNames
     * @param bool $clearErrors
     *
     * @param bool $self_only
     *
     * @return bool
     */
    public function validate($attributeNames = null, $clearErrors = true, $self_only = false)
    {
        if ($attributeNames !== null) {
            $attributeNames = (array)$attributeNames;
        }

        if ($attributeNames !== null) {
            $parentNames = array_filter($attributeNames, '\is_string');
            $success = $parentNames ? parent::validate($parentNames, $clearErrors) : true;
        } else {
            $success = parent::validate(null, $clearErrors);
        }
        if (!$self_only) {
            foreach ($this->models as $name => $model) {
                if ($attributeNames === null || array_key_exists($name, $attributeNames) || \in_array($name, $attributeNames, true)) {
                    $innerNames = ArrayHelper::getValue($attributeNames, $name);
                    $form = $this->$name;
                    if (\is_array($form)) {
                        $success = Model::validateMultiple($form, $innerNames) && $success;
                    } else {
                        $success = $form->validate($innerNames, $clearErrors) && $success;
                    }
                }
            }
        }
        return $success;
    }

}