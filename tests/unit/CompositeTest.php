<?php

namespace tests\unit;

use Codeception\Test\Unit;
use Yii;

/**
 *
 */
class CompositeTest extends Unit
{

    public function testKeyStorageSet()
    {
        Yii::$app->keyStorage->set('test.key', 'testValue');
        $this->assertEquals('testValue', Yii::$app->keyStorage->get('test.key', null, false));
        Yii::$app->keyStorage->set('test.key', 'anotherTestValue');
        $this->assertEquals('anotherTestValue', Yii::$app->keyStorage->get('test.key', null, false));
    }

    /**
     * @depends testKeyStorageSet
     */
    public function testKeyStorageHas()
    {

        $this->assertFalse(Yii::$app->keyStorage->has('falseKey'));
    }

    /**
     * @depends testKeyStorageHas
     */
    public function testKeyStorageRemove()
    {
        Yii::$app->keyStorage->remove('test.key');
        $this->assertNull(Yii::$app->keyStorage->get('test.key', null, false));
        $this->assertFalse(Yii::$app->keyStorage->has('test.key', false));
    }
}
