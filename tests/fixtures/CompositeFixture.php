<?php

namespace tests\fixtures;


use yii\test\ArrayFixture;

/**
 * User fixture
 */
class CompositeFixture extends ArrayFixture
{

    public $dataFile = 'data/composite.php';
}
