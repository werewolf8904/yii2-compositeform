<?php

use yii\db\Migration;

/**
 * Class m180806_201321_add_test_product_image
 */
class m180806_201321_add_test_product_image extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        //catalog product images
        $this->createTable('{{%catalog_product_images}}', [
            'id' => $this->primaryKey(),
            'catalog_product_id' => $this->integer()->notNull(),
            'image' => $this->string()->notNull(),
            'sort' => $this->integer()->defaultValue(0)->notNull(),
        ], $tableOptions);

        $this->addForeignKey('fk_catalog_product_images_2_catalog_product',
            '{{%catalog_product_images}}', 'catalog_product_id',
            '{{%catalog_product}}', 'id',
            'CASCADE', 'CASCADE');

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%catalog_product_images}}');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180806_201321_add_test_product_image cannot be reverted.\n";

        return false;
    }
    */
}
