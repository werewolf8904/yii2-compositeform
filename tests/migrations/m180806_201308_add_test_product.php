<?php

use yii\db\Migration;

/**
 * Class m180806_201308_add_test_product
 */
class m180806_201308_add_test_product extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('{{%catalog_product}}', [
            'id' => $this->primaryKey(),
            'status' => $this->smallInteger()->notNull(),
            'sku' => $this->string()->notNull(),
            'sort' => $this->integer()->defaultValue(0)->notNull(),
            'base_price' => $this->money(10, 2)->notNull(),
            'quantity' => $this->integer()->notNull()->defaultValue(0),
            'id_1c' => $this->string(200)->notNull(),
        ], $tableOptions);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%catalog_product}}');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180806_201308_add_test_product cannot be reverted.\n";

        return false;
    }
    */
}
