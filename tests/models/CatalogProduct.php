<?php

namespace test\models;


use yii\db\ActiveQuery;
use yii\db\ActiveRecord;


/**
 * This is the model class for table "{{%catalog_product}}".
 *
 * @property integer                $id
 * @property integer                $status
 * @property integer                $sort
 * @property string                 $price
 * @property integer                $quantity
 * @property string                 $sku
 * @property integer                $created_at
 * @property integer                $updated_at
 * @property string                 $slug
 * @property bool                   $is_top_sale
 * @property integer                $catalog_outofstock_status_id
 * @property integer                $catalog_manufacturer_id
 * @property string                 $id_1c [varchar(200)]
 *
 * @property CatalogProductImages   $image
 * @property CatalogProductImages[] $images
 * @property CatalogProduct[]       $productsRelated
 * @property mixed                  $route
 * @property mixed                  feedPrice
 *
 */
class CatalogProduct extends ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName(): string
    {
        return '{{%catalog_product}}';
    }

    /**
     * @inheritdoc
     */
    public function rules(): array
    {
        return [
            [['base_price'], 'required'],
            [['id_1c'], 'unique'],
            [['sku'], 'default', 'value' => null],
            [['status', 'sort', 'quantity'], 'integer'],
        ];
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getImages(): ActiveQuery
    {
        return $this->hasMany(CatalogProductImages::class, ['catalog_product_id' => 'id']);
    }


}
