<?php

namespace test\models;


use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%catalog_product_images}}".
 *
 * @property integer        $id
 * @property integer        $catalog_product
 * @property string         $image
 * @property boolean        $is_default
 * @property integer        $sort
 *
 * @property CatalogProduct $catalogProduct
 */
class CatalogProductImages extends ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%catalog_product_images}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['sort'], 'integer'],
            [['sort'], 'default', 'value' => 0],
            [['image'], 'string', 'max' => 255],
            [['catalog_product_id'], 'exist', 'skipOnError' => true, 'targetClass' => CatalogProduct::class, 'targetAttribute' => ['catalog_product_id' => 'id']]
        ];
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCatalogProduct()
    {
        return $this->hasOne(CatalogProduct::class, ['id' => 'catalog_product_id']);
    }


}
