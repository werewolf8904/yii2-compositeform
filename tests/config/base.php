<?php
/**
 * Application configuration shared by all applications and test types
 */
return [
    'id' => 'app-common',
    'basePath' => YII_APP_BASE_PATH,
    'components' => [
        'db' => [
            'class' => \yii\db\Connection::class,
            'dsn' => getenv('TEST_DB_DSN'),
            'username' => getenv('TEST_DB_USERNAME'),
            'password' => getenv('TEST_DB_PASSWORD')
        ],
    ],


];
